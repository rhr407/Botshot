
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %matplotlib inline
plt.style.use('ggplot')

import xgboost as xgb

import pickle
import gc
import os
import sys


global sess
global graph


from keras import applications
from keras import backend as K
from keras import layers
from keras import models
from keras import optimizers
import tensorflow as tf

from tensorflow.python.keras.backend import set_session
from tensorflow.python.keras.models import load_model


import time


from keras.layers import BatchNormalization

from keras.layers import LeakyReLU

from sklearn.svm import SVC

from sklearn.ensemble import RandomForestClassifier

from sklearn.neighbors import KNeighborsClassifier

#Import Gaussian Naive Bayes model
from sklearn.naive_bayes import GaussianNB

# Import `Sequential` from `keras.models`
from keras.models import Sequential

# Import `Dense` from `keras.layers`
from keras.layers import Dense

# Load libraries
from sklearn.tree import DecisionTreeClassifier # Import Decision Tree Classifier
from sklearn.model_selection import train_test_split # Import train_test_split function
from sklearn import metrics #Import scikit-learn metrics module for accuracy calculation
from sklearn.linear_model import LogisticRegression




DEBUG = 0




from sklearn.metrics import recall_score, precision_score, roc_auc_score

def recall(preds, dtrain):
    labels = dtrain.get_label()
    return 'recall',  recall_score(labels, np.round(preds))

def precision(preds, dtrain):
    labels = dtrain.get_label()
    return 'precision',  precision_score(labels, np.round(preds))

def roc_auc(preds, dtrain):
    labels = dtrain.get_label()
    return 'roc_auc',  roc_auc_score(labels, preds)






def BaseMetrics(y_pred,y_test):
    TP = np.sum( (y_pred == 1) & (y_test == 1) )
    TN = np.sum( (y_pred == 0) & (y_test == 0) )
    FP = np.sum( (y_pred == 1) & (y_test == 0) )
    FN = np.sum( (y_pred == 0) & (y_test == 1) )
    return TP, TN, FP, FN

def SimpleMetrics(y_pred,y_test):
    TP, TN, FP, FN = BaseMetrics(y_pred,y_test)
    ACC = ( TP + TN ) / ( TP + TN + FP + FN )
    
    # Reporting
    from IPython.display import display
    print( 'Confusion Matrix')
    display( pd.DataFrame( [[TN,FP],[FN,TP]], columns=['Pred 0','Pred 1'], index=['True 0', 'True 1'] ) )
    print( 'Accuracy : {}'.format( ACC ))
    
def SimpleAccuracy(y_pred,y_test):
    TP, TN, FP, FN = BaseMetrics(y_pred,y_test)
    ACC = ( TP + TN ) / ( TP + TN + FP + FN )
    return ACC

def c(y_pred,y_test):
    TP, TN, FP, FN = BaseMetrics(y_pred,y_test)
    RCL = ( TP ) / ( TP +  FN )
    return RCL


    
def get_data_batch(train, batch_size, seed):
    # # random sampling - some samples will have excessively low or high sampling, but easy to implement
    # np.random.seed(seed)
    # x = train.loc[ np.random.choice(train.index, batch_size) ].values

    # print("seed is ======>>>> " + str(seed))
    # iterate through shuffled indices, so every sample gets covered evenly
    start_i = (batch_size * seed) % len(train)
    # print("start_i is ======>>>> " + str(start_i))

    stop_i = start_i + batch_size
    # print("stop_i is ======>>>> " + str(stop_i))

    shuffle_seed = (batch_size * seed) // len(train)
    # print("shuffle_seed is ======>>>> " + str(shuffle_seed))

    np.random.seed(shuffle_seed)
    # wasteful to shuffle every time
    train_ix = np.random.choice(list(train.index), replace=False, size=len(train))
    # duplicate to cover ranges past the end of the set
    train_ix = list(train_ix) + list(train_ix)
    x = train.loc[train_ix[start_i: stop_i]].values

    return_matrix = np.reshape(x, (batch_size, -1))

    # print(return_matrix)

    return return_matrix


    
def CheckAccuracy(x, g_z, data_cols, label_cols=[], seed=0, with_class=False, data_dim=2, classifier = 'xgb'):

    # Slightly slower code to create dataframes to feed into the xgboost
    # dmatrix formats

    # real_samples = pd.DataFrame(x, columns=data_cols+label_cols)
    # gen_samples = pd.DataFrame(g_z, columns=data_cols+label_cols)
    # real_samples['syn_label'] = 0
    # gen_samples['syn_label'] = 1

    # training_fraction = 0.5
    # n_real, n_gen = int(len(real_samples)*training_fraction), int(len(gen_samples)*training_fraction)
    # train_df = pd.concat([real_samples[:n_real],gen_samples[:n_gen]],axis=0)
    # test_df = pd.concat([real_samples[n_real:],gen_samples[n_gen:]],axis=0)

    # X_col = test_df.columns[:-1]
    # y_col = test_df.columns[-1]
    # dtrain = xgb.DMatrix(train_df[X_col], train_df[y_col], feature_names=X_col)
    # dtest = xgb.DMatrix(test_df[X_col], feature_names=X_col)
    # y_test = test_df['syn_label']

    # print("x shape is =====>>>> " + str(x.shape))
    # print("g_z shape is =====>>>> " + str(g_z.shape))


    # Use half of each real and generated set for training
    dtrain = np.vstack([x[:int(len(x) / 2)], g_z[:int(len(g_z) / 2)]])

    # synthetic labels
    dlabels = np.hstack([np.zeros(int(len(x) / 2)), np.ones(int(len(g_z) / 2))])

    # Use the other half of each set for testing
    dtest = np.vstack([x[int(len(x) / 2):], g_z[int(len(g_z) / 2):]])
    y_test = dlabels  # Labels for test samples will be the same as the labels for training samples, assuming even batch sizes


    y_pred = 0


    if(classifier=='XGB'):

        if DEBUG:

            print('Evaluation ---->> XBG >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        dtrain = xgb.DMatrix(dtrain, dlabels, feature_names=data_cols + label_cols)
        dtest = xgb.DMatrix(dtest, dlabels, feature_names=data_cols + label_cols)

        xgb_params = {
            # 'tree_method': 'hist', # for faster evaluation
            'max_depth': 4,  # for faster evaluation
            'objective': 'binary:logistic',
            'random_state': 0,
            'eval_metric': 'auc',  # allows for balanced or unbalanced classes
        }
        results_dict = {}

        # limit to ten rounds for faster evaluation
        # xgb_test = xgb.train(xgb_params, dtrain, num_boost_round=10)
        xgb_test = xgb.train(xgb_params, dtrain, num_boost_round=100, 
                         verbose_eval=False,
                         early_stopping_rounds=20, 
                         evals=[(dtrain,'train'),(dtest,'test')],
                         evals_result = results_dict,     
                         feval = recall, maximize=True)

         

        y_pred = np.round(xgb_test.predict(dtest))

    elif (classifier=='SVM'):

        if DEBUG:
            print('Evaluation ---->> SVM >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        svclassifier = SVC(kernel='linear')
        svclassifier.fit(dtrain, y_test)
        y_pred = svclassifier.predict(dtest)

    elif (classifier=='DT'):

        if DEBUG:

            print('Evaluation ---->> DT >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

                # Create Decision Tree classifer object
        clf = DecisionTreeClassifier()
        # Train Decision Tree Classifer
        clf = clf.fit(dtrain,y_test)
        #Predict the response for test dataset
        y_pred = clf.predict(dtest)

    elif (classifier=='RF'):

        if DEBUG:

            print('Evaluation ---->> RF >>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        #Import Random Forest Model
        #Create a Gaussian Classifier
        clf=RandomForestClassifier(n_estimators=100)
        #Train the model using the training sets y_pred=clf.predict(X_test)
        clf.fit(dtrain,y_test)
        y_pred=clf.predict(dtest)

    elif (classifier=='LR'):
        
        if DEBUG:
            print('Evaluation ---->> LR >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        logreg = LogisticRegression()
        logreg.fit(dtrain, y_test)
        y_pred = logreg.predict(dtest)

    elif (classifier=='KNN'):
        
        if DEBUG:

            print('Evaluation ---->> KNN >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        knn_classifier = KNeighborsClassifier(n_neighbors=5)
        knn_classifier.fit(dtrain, y_test)
        y_pred = knn_classifier.predict(dtest)

    elif (classifier=='NB'):

        if DEBUG:
            print('Evaluation ---->> NB >>>>>>>>>>>>>>>>>>>>>>>>>>>>')

        #Create a Gaussian Classifier
        gnb = GaussianNB()
        #Train the model using the training sets
        gnb.fit(dtrain, y_test)
        #Predict the response for test dataset
        y_pred = gnb.predict(dtest)


    # return '{:.2f}'.format(SimpleAccuracy(y_pred, y_test)) # assumes
    # balanced real and generated datasets
    # assumes balanced real and generated datasets

    # SimpleMetrics(y_pred, y_test)
    return SimpleAccuracy(y_pred, y_test)
    # return SimpleRecall(y_pred, y_test)

def PlotData(x, g_z, data_cols, label_cols=[], seed=0, with_class=False, 
            data_dim=2, save=False, prefix='', list_batch_iteration=[] , 
            xgb_losses = [], svm_losses = [], dt_losses =[], nb_losses =[], 
            knn_losses = [], rf_losses = [], lr_losses = [] ,
            list_disc_loss_real = [], list_disc_loss_generated = [], 
            list_combined_loss = [],  list_disc_perf = [], log_iteration=0, list_log_iteration = [], figs_path  = ''):

# def PlotData(x, g_z, data_cols, label_cols=[], seed=0, with_class=False, 
#             data_dim=2, save=False, prefix='', list_batch_iteration=[] , 
#             list_disc_loss_real = [], 
#             list_disc_loss_generated = [], list_combined_loss = [], 
#             list_disc_perf = []):

    print(label_cols)


    real_samples = pd.DataFrame(x, columns=data_cols + label_cols)
    gen_samples = pd.DataFrame(g_z, columns=data_cols + label_cols)

    if DEBUG:
        print("Shape of real Samples is =" + str(real_samples.shape))
        print("Shape of gen Samples is =" + str(gen_samples.shape))

# -------------------------------------------------------------------------------------------
    
    
    # f, single_feature_arr = plt.subplots(1, 1, figsize=(10, 3))


    # single_feature_arr.set_xlabel('Value')  # Add x label to all plots
    # single_feature_arr.set_ylabel('Counts')  # Add x label to all plots


    # single_feature_arr.hist( real_samples['Fwd IAT Max'] , label=['Real ' + 'Fwd IAT Max'], color=['#CC2529'], alpha=0.4,
    #                       bins=np.linspace( np.percentile(real_samples['Fwd IAT Max'],0), np.percentile(real_samples['Fwd IAT Max'],100),100 ),
    #                       density=True )

    # single_feature_arr.hist( gen_samples['Fwd IAT Max'] , label=['Generated ' + 'Fwd IAT Max'], color=['#4682B4'], alpha=0.6,
    #                       bins=np.linspace( np.percentile(gen_samples['Fwd IAT Max'],0), np.percentile(gen_samples['Fwd IAT Max'],100),100 ),
    #                       density=True )
    # plt.legend()



# ==================================================================================================================

    f, single_feature_arr = plt.subplots(1, 1, figsize=(5, 3))


    single_feature_arr.set_xlabel('Value')  # Add x label to all plots
    single_feature_arr.set_ylabel('Counts')  # Add x label to all plots

# ==================================================================================================================

    single_feature_arr.hist( real_samples['Pkt Len Std'] , label=['Real ' + 'Pkt Len Std'], color=['#CC2529'], alpha=0.4,
                          bins=np.linspace( np.percentile(real_samples['Fwd IAT Max'],0), np.percentile(real_samples['Fwd IAT Max'],100), 100),
                          density=True )

    single_feature_arr.hist( gen_samples['Pkt Len Std'] , label=['Generated ' + 'Pkt Len Std'], color=['#4682B4'], alpha=0.6,
                          bins=np.linspace( np.percentile(gen_samples['Fwd IAT Max'],0), np.percentile(gen_samples['Fwd IAT Max'],100), 100 ),
                          density=True )
    
    plt.legend()

    plt.savefig(figs_path + 'Probability Density Functions-' + str(list_log_iteration[-1]) + '.svg', format='svg')

    plt.close(f)


# ==================================================================================================================

# Plot the data by each feature

# print(data.describe())

    # axarr = [[]]*len(data_cols)
    # columns = 4
    # rows = int( np.ceil( len(data_cols) / columns ) )
    # f, fig = plt.subplots( figsize=(columns*3.5, rows*2) )

    # f.suptitle('Real & Generated Feature Value Counts', size=16)

    # for i, col in enumerate(data_cols[:]):
    #     # axarr[i] = plt.subplot2grid( (int(rows), int(columns)), (int(i//columns), int(i%columns)) )
    #     axarr[i].hist(real_samples[data_cols[col]]  , label=['Real'], color=('#009933'), alpha=0.4,
    #                           bins=np.linspace( np.percentile(real_samples[col],0), np.percentile(real_samples[col],100),50 ),
    #                           density=True )
        
    #     axarr[i].hist( gen_samples[data_cols[col]] , label=['Generated'], color=['#CC2529'], alpha=0.4,
    #                           bins=np.linspace( np.percentile(gen_samples[col],0), np.percentile(gen_samples[col],100),50 ),
    #                           density=True )
    #     axarr[i].set_xlabel(col, size=12)
    #     axarr[i].set_ylim([0,0.8])
    #     axarr[i].tick_params(axis='both', labelsize=10)
    #     if i == 0: 
    #         legend = axarr[i].legend()
    #         legend.get_frame().set_facecolor('white')
    #     if i%4 != 0 : 
    #         axarr[i].tick_params(axis='y', left='off', labelleft='off')
    #     else:
    #         axarr[i].set_ylabel('Fraction',size=12)

    # plt.tight_layout(rect=[0,0,1,0.95]) # xmin, ymin, xmax, ymax
    # # plt.savefig('plots/Engineered_Data_Distributions.png')
    # plt.show()

# ==================================================================================================================



    f, img_array = plt.subplots(1, 2, figsize=(10, 3))

    # img_array[0].scatter(real_samples['Pkt Len Std'], real_samples['Fwd IAT Max'])  # , cmap='plasma'  )
    # img_array[0].set_title('real')

    # img_array[1].scatter(gen_samples['Pkt Len Std'], gen_samples['Fwd IAT Max'])  # , cmap='plasma'  )
    # img_array[1].set_title('generated')

    if with_class == True:

        img_array[0].scatter(real_samples['Pkt Len Std'], real_samples['Fwd IAT Max'], c =real_samples[label_cols[0]] / 2)  # , cmap='plasma'  )
        img_array[0].set_title('real')

        img_array[1].scatter(gen_samples['Pkt Len Std'], gen_samples['Fwd IAT Max'], c =real_samples[label_cols[0]] / 2)  # , cmap='plasma'  )
        img_array[1].set_title('generated')



    else:
        img_array[0].scatter(real_samples['Pkt Len Std'], real_samples['Fwd IAT Max'])  # , cmap='plasma'  )
        img_array[0].set_title('real')

        img_array[1].scatter(gen_samples['Pkt Len Std'], gen_samples['Fwd IAT Max'])  # , cmap='plasma'  )
        img_array[1].set_title('generated')



    
    img_array[0].set_ylabel('Fwd IAT Max')  # Only add y label to left plot
    

    for a in [0,1]:
        img_array[a].set_xlabel(data_cols[2*(a//2)])  # Add x label to all plots
        

    img_array[1].set_xlim(img_array[0].get_xlim()), img_array[1].set_ylim(img_array[0].get_ylim())  # Use axes ranges from real data for generated data


    plt.savefig(figs_path + 'Scatter-of-Generated-Data-' + str(list_log_iteration[-1]) + '.svg', format='svg')

    plt.close(f)


    f, loss_img_array = plt.subplots(1, 1, figsize=(10, 6))


    loss_img_array.set_xlabel('epochs')  # Add x label to all plots
    loss_img_array.set_ylabel('values')  # Add x label to all plots

    # print(len(list_batch_iteration))
    # print(len(xgb_losses))

# ==================================================================================================================
    # plt.plot(list_log_iteration, xgb_losses, color='tab:blue', marker='o', label='XGB Acc')
    # plt.plot(list_log_iteration, xgb_losses, linestyle='solid' , color='blue')

    # plt.plot(list_log_iteration, svm_losses, color='tab:green', marker='^', label='SVM Acc')
    # plt.plot(list_log_iteration, svm_losses, linestyle='solid' , color='green')

    # plt.plot(list_log_iteration, dt_losses, color='tab:olive', marker='v', label='DT Acc')
    # plt.plot(list_log_iteration, dt_losses, linestyle='solid' , color='yellow')

    # plt.plot(list_log_iteration, nb_losses, color='tab:orange', marker='s', label='NB Acc')
    # plt.plot(list_log_iteration, nb_losses, linestyle='solid' , color='orange')

    # plt.plot(list_log_iteration, knn_losses, color='tab:purple', marker='P', label='KNN Acc')
    # plt.plot(list_log_iteration, knn_losses, linestyle='solid' , color='purple')

    # plt.plot(list_log_iteration, rf_losses, color='tab:brown', marker='x', label='RF Acc')
    # plt.plot(list_log_iteration, rf_losses, linestyle='solid' , color='brown')

    # plt.plot(list_log_iteration, lr_losses, color='tab:grey', marker='H', label='LR Acc')
    # plt.plot(list_log_iteration, lr_losses, linestyle='solid' , color='grey')



    plt.plot(list_log_iteration, xgb_losses, linestyle='solid' , color='blue', label='XGB Acc')
    plt.plot(list_log_iteration, dt_losses, linestyle='solid' , color='yellow', label='DT Acc')
    plt.plot(list_log_iteration, nb_losses, linestyle='solid' , color='orange', label='NB Acc')
    plt.plot(list_log_iteration, knn_losses, linestyle='solid' , color='purple', label='KNN Acc')
    plt.plot(list_log_iteration, rf_losses, linestyle='solid' , color='brown', label='RF Acc')
    plt.plot(list_log_iteration, lr_losses, linestyle='solid' , color='magenta', label='LR Acc')
    plt.plot(list_log_iteration, svm_losses, linestyle='solid' , color='green', label='SVM Acc')



    best_xgboost = list(xgb_losses).index( xgb_losses.min())
    plt.axvline(x=best_xgboost, color='blue', label='XGB(min) = {} at iteration = {}'.format(round(xgb_losses.min(),2), best_xgboost))

    best_svm = list(svm_losses).index( svm_losses.min())
    plt.axvline(x=best_svm, color='green', label='SVM(min) = {} at iteration = {}'.format(round(svm_losses.min(),2), best_svm))

    best_dt = list(dt_losses).index( dt_losses.min())
    plt.axvline(x=best_dt, color='yellow', label='DT(min) = {} at iteration = {}'.format(round(dt_losses.min(),2), best_dt))

    best_nb = list(nb_losses).index( nb_losses.min())
    plt.axvline(x=best_nb, color='orange', label='NB(min) = {} at iteration = {}'.format(round(nb_losses.min(),2), best_nb))

    best_knn = list(knn_losses).index( knn_losses.min())
    plt.axvline(x=best_knn, color='purple', label='KNN(min) = {} at iteration = {}'.format(round(knn_losses.min(),2), best_knn))

    best_rf = list(rf_losses).index( rf_losses.min())
    plt.axvline(x=best_rf, color='brown', label='RF(min) = {} at iteration = {}'.format(round(rf_losses.min(),2), best_rf))

    best_lr = list(lr_losses).index( lr_losses.min())
    plt.axvline(x=best_lr, color='magenta', label='LR(min) = {} at iteration = {}'.format(round(lr_losses.min(),2), best_lr))


    # loss_img_array.plot(list_log_iteration, xgb_losses, label='XGB Acc')  # , cmap='plasma'  )

    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=False, ncol=7)
    plt.legend(ncol=2)

    plt.savefig(figs_path + 'Evaluation_of_Generated_Data-' + str(list_log_iteration[-1]) + '.svg', format='svg')

    plt.close(f)

    # loss_img_array.plot(list_log_iteration, svm_losses, color='tab:orange', linestyle='r--', label='SVM Acc')  # , cmap='plasma'  )
    # loss_img_array.plot(list_log_iteration, dt_losses, linestyle='g--', , color='tab:orange''bs', label='DT Acc')  # , cmap='plasma'  )
    # loss_img_array.plot(list_log_iteration, nb_losses, linestyle='b--', , color='tab:orange''g^', label='NB Acc')  # , cmap='plasma'  )
    # loss_img_array.plot(list_log_iteration, knn_losses, linestyle='o--', , color='tab:orange''yv', label='KNN Acc')  # , cmap='plasma'  )
    # loss_img_array.plot(list_log_iteration, rf_losses, linestyle='y--', , color='tab:orange''m+', label='RF Acc')  # , cmap='plasma'  )
    # loss_img_array.plot(list_log_iteration, lr_losses, linestyle='m--', , color='tab:orange''cD', label='LR Acc')  # , cmap='plasma'  )

    f, loss_img_array = plt.subplots(1, 1, figsize=(10, 6))


    loss_img_array.set_xlabel('batch_iteration')  # Add x label to all plots
    loss_img_array.set_ylabel('values')  # Add x label to all plots

    loss_img_array.plot(list_batch_iteration, list_combined_loss, label = 'L[G(z)]')  # , cmap='plasma'  )
    loss_img_array.plot(list_batch_iteration, list_disc_loss_real, label = 'L[D(x)]')  # , cmap='plasma'  )
    loss_img_array.plot(list_batch_iteration, list_disc_loss_generated, label = 'L[D(G(z))]')  # , cmap='plasma'  )
    loss_img_array.plot(list_batch_iteration, list_disc_perf, label = 'L[D(x)] - L[D(G(z))]')  # , cmap='plasma'  )
    
    # plt.legend(loc='upper left', bbox_to_anchor=(0.5, -0.05), shadow=False, ncol=4)



    plt.legend()

    plt.savefig(figs_path + 'GAN Losses-' + str(list_log_iteration[-1]) + '.svg', format='svg')

    plt.close(f)
 

#### Functions to define the layers of the networks used in the 'define_models' functions below
    
def generator_network(x, data_dim, base_n_count): 
                                                        # L_input
    
    x = layers.Dense(base_n_count*1, activation='relu')(x)#L1
    # x = layers.BatchNormalization() (x)   #L2

    x = layers.Dense(base_n_count*2, activation='relu')(x)#L3
    x = layers.BatchNormalization() (x)   #L4

    x = layers.Dense(base_n_count*4, activation='relu')(x)#L5
    x = layers.BatchNormalization() (x) #L6

    x = layers.Dense(base_n_count*8, activation='relu')(x)#L7
    x = layers.BatchNormalization() (x) #L8




    x = layers.Dense(data_dim, activation='relu')(x)#L_output

    return x
    
def generator_network_w_label(x, labels, data_dim, label_dim, base_n_count): 
    x = layers.concatenate([x,labels])#L_input

    x = layers.Dense(base_n_count*1, activation='relu')(x) # L1
    x = layers.BatchNormalization() (x)   #L2

    x = layers.Dense(base_n_count*2, activation='relu')(x) # L3
    x = layers.BatchNormalization() (x) #L4  

    x = layers.Dense(base_n_count*4, activation='relu')(x)#L5
    x = layers.BatchNormalization() (x)  #lL6

    x = layers.Dense(base_n_count*8, activation='relu')(x)#L7
    x = layers.BatchNormalization() (x) #L8


    x = layers.Dense(data_dim, activation='relu')(x) #L9
   
    x = layers.concatenate([x,labels])#L_output
    return x
    
def discriminator_network(x, data_dim, base_n_count):   

                                                            #L_input
 
    x = layers.Dense(base_n_count*8, activation='relu')(x)#L_1
    x = layers.BatchNormalization() (x) #L_2 
    # x = layers.Dropout(0.5)(x)


    x = layers.Dense(base_n_count*4, activation='relu')(x)#L_3
    x = layers.BatchNormalization() (x)  #L_4
    # x = layers.Dropout(0.5)(x)

    x = layers.Dense(base_n_count*2, activation='relu')(x)#L_5
    x = layers.BatchNormalization() (x)#L_6
    # x = layers.Dropout(0.5)(x)

    x = layers.Dense(base_n_count*1, activation='relu')(x)#L_7
    x = layers.BatchNormalization() (x) #L_8
    # x = layers.Dropout(0.3)(x)  

    x = layers.Dense(1, activation='sigmoid')(x)#L_output

    return x
    
def critic_network(x, data_dim, base_n_count):


                                                            #L_input
    x = layers.Dense(base_n_count*8, activation='relu')(x)#L_1
    x = layers.BatchNormalization() (x)  #L_2
    # x = layers.Dropout(0.1)(x)


    x = layers.Dense(base_n_count*4, activation='relu')(x)#L_3
    x = layers.BatchNormalization() (x)  #L_4
    # x = layers.Dropout(0.1)(x)

    x = layers.Dense(base_n_count*2, activation='relu')(x)#L_5
    x = layers.BatchNormalization() (x)#L_6
    # x = layers.Dropout(0.1)(x)

    x = layers.Dense(base_n_count*1, activation='relu')(x)#L_7
    x = layers.BatchNormalization() (x) #L_8
    # x = layers.Dropout(0.1)(x)  

    x = layers.Dense(1, activation='sigmoid')(x)#L_output
    return x

    
    
#### Functions to define the keras network models    
    
def define_models_GAN(rand_dim, data_dim, base_n_count, type=None):
    generator_input_tensor = layers.Input(shape=(rand_dim, ))
    generated_image_tensor = generator_network(generator_input_tensor, data_dim, base_n_count)

    generated_or_real_image_tensor = layers.Input(shape=(data_dim,))

    if type == 'Wasserstein':
        discriminator_output = critic_network(generated_or_real_image_tensor, data_dim, base_n_count)
    else:
        discriminator_output = discriminator_network(generated_or_real_image_tensor, data_dim, base_n_count)

    generator_model = models.Model(inputs=[generator_input_tensor], outputs=[generated_image_tensor], name='generator')
    discriminator_model = models.Model(inputs=[generated_or_real_image_tensor], outputs=[discriminator_output], name='discriminator')

    combined_output = discriminator_model(generator_model(generator_input_tensor))
    combined_model = models.Model(inputs=[generator_input_tensor], outputs=[combined_output], name='combined')

    return generator_model, discriminator_model, combined_model

def define_models_CGAN(rand_dim, data_dim, label_dim, base_n_count, type=None):
    generator_input_tensor = layers.Input(shape=(rand_dim, ))
    labels_tensor = layers.Input(shape=(label_dim,))  # updated for class
    generated_image_tensor = generator_network_w_label(generator_input_tensor, labels_tensor, data_dim, label_dim, base_n_count)  # updated for class

    generated_or_real_image_tensor = layers.Input(shape=(data_dim + label_dim,))  # updated for class

    if type == 'Wasserstein':
        discriminator_output = critic_network(generated_or_real_image_tensor, data_dim + label_dim, base_n_count)  # updated for class
    else:
        discriminator_output = discriminator_network(generated_or_real_image_tensor, data_dim + label_dim, base_n_count)  # updated for class

    generator_model = models.Model(inputs=[generator_input_tensor, labels_tensor], outputs=[
                                   generated_image_tensor], name='generator')  # updated for class

    discriminator_model = models.Model(inputs=[generated_or_real_image_tensor],
                                       outputs=[discriminator_output],
                                       name='discriminator')

    combined_output = discriminator_model(generator_model(
        [generator_input_tensor, labels_tensor]))  # updated for class
    combined_model = models.Model(inputs=[generator_input_tensor, labels_tensor], outputs=[
                                  combined_output], name='combined')  # updated for class

    return generator_model, discriminator_model, combined_model


def em_loss(y_coefficients, y_pred):
    # define earth mover distance (wasserstein loss)
    # literally the weighted average of the critic network output
    # this is defined separately so it can be fed as a loss function to the
    # optimizer in the WGANs
    return tf.reduce_mean(tf.multiply(y_coefficients, y_pred))


def train_discriminator_step(model_components, seed=0):
    
    [ cache_prefix, with_class, starting_step,
                        train, data_cols, data_dim,
                        label_cols, label_dim,
                        generator_model, discriminator_model, combined_model,
                        rand_dim, nb_steps, batch_size,
                        k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
                        data_dir, FIGS_PATH, generator_model_path, discriminator_model_path,
                        sess, _z, _x, _labels, _g_z, epsilon, x_hat, gradients, _gradient_penalty,
                        _disc_loss_generated, _disc_loss_real, _disc_loss, disc_optimizer,
                        show, combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, test_size, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses, test_size, device
                        ] = model_components


    sess.run(tf.compat.v1.global_variables_initializer())

    if with_class:

        d_l_g, d_l_r, _ = sess.run([_disc_loss_generated, _disc_loss_real, disc_optimizer], feed_dict={_z: np.random.normal(size=(batch_size, rand_dim)), _x:get_data_batch(train, batch_size, seed=seed),
            # .reshape(-1,label_dim), # updated for class
            _labels: get_data_batch(train, batch_size, seed=seed)[:, -label_dim:],
            epsilon: np.random.uniform(low=0.0, high=1.0, size=(batch_size, 1))
        })
        
    else:

        d_l_g, d_l_r, _ = sess.run([_disc_loss_generated, _disc_loss_real, disc_optimizer], feed_dict={_z: np.random.normal(size=(batch_size, rand_dim)),_x: get_data_batch(train,batch_size, seed=seed), epsilon: np.random.uniform(low=0.0, high=1.0, size=(batch_size, 1))
        })
    return d_l_r, d_l_g


#### Functions specific to the vanilla GAN architecture   
        
def training_steps_GAN(model_components):
    
    [ cache_prefix, with_class, starting_step,
                        train, data_cols, data_dim,
                        label_cols, label_dim,
                        generator_model, discriminator_model, combined_model,
                        rand_dim, nb_steps, batch_size, 
                        k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
                        data_dir, FIGS_PATH, generator_model_path, discriminator_model_path, show,
                        combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, 
                        nb_losses, knn_losses, rf_losses, lr_losses, test_size, device ] = model_components  
    
    
    batch_iteration = 0
    total_time_so_far = 0

    list_batch_iteration = []

    list_log_iteration = []

    list_disc_loss_real = []
    list_disc_loss_generated = []
    list_combined_loss = []
    list_disc_perf = []

    log_iteration = 0

    start_log_time = time.time()


    #---------------------------------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------------------------------------
    if DEBUG:

        print("======================================================")
        print("Batch Size Selected -------->>>>>>> " + str(batch_size))
        print("======================================================")




    for i in range(starting_step, starting_step+nb_steps):

        with tf.device("/gpu:1"):

            K.set_learning_phase(0) # 0 = set this as test because we are going to predict from generator

            # print('Training Disciminator----------------->>>\n')
            for j in range(k_d):
                np.random.seed(i+j)
                z = np.random.normal(size=(batch_size, rand_dim), scale=0.02)
                x = get_data_batch(train, batch_size, seed=i+j)
                
                if with_class:
                    labels = x[:, -label_dim:]
                    g_z = generator_model.predict([z, labels])
                else:
                    g_z = generator_model.predict(z)

                K.set_learning_phase(1) # 1 = train


                d_l_r = discriminator_model.train_on_batch(x, np.random.uniform(low=0.999, high=1.0, size=batch_size )) # 0.7, 1.2 # GANs need noise to prevent loss going to zero
                d_l_g = discriminator_model.train_on_batch(g_z, np.random.uniform(low=0.0, high=0.001, size=batch_size)) # 0.0, 0.3 # GANs need noise to prevent loss going to zero
                # d_l_r = discriminator_model.train_on_batch(x, np.ones(batch_size)) # without noise
                # d_l_g = discriminator_model.train_on_batch(g_z, np.zeros(batch_size)) # without noise
            disc_loss_real.append(d_l_r)
            disc_loss_generated.append(d_l_g)
            

            # print('Training Generator----------------->>>\n\n')
            for j in range(k_g):
                np.random.seed(i + j)
                z = np.random.normal(size=(batch_size, rand_dim), scale=0.02)
                if with_class:
                    # loss = combined_model.train_on_batch([z, labels],
                    # np.ones(batch_size)) # without noise
                    loss = combined_model.train_on_batch([z, labels], np.random.uniform(low=0.999, high=1.0, size=batch_size))  # 0.7, 1.2 # GANs need noise to prevent loss going to zero
                else:
                    # loss = combined_model.train_on_batch(z, np.ones(batch_size))
                    # # without noise
                    # 0.7, 1.2 # GANs need noise to prevent loss going to zero
                    loss = combined_model.train_on_batch(z, np.random.uniform(low=0.999, high=1.0, size=batch_size))
            
            combined_loss.append(loss)
            
                
            list_batch_iteration.append(batch_iteration)

            if DEBUG:

                print('Current Batch Number: ' + str(batch_iteration))

            batch_iteration = batch_iteration + 1    

            list_disc_loss_real.append(d_l_r)
            list_disc_loss_generated.append(d_l_g)
            list_combined_loss.append(loss)            
            list_disc_perf.append(d_l_r - d_l_g) 

        

#---------------------------------------------------------------------------------------------------------------
          
                                    # Determine xgb loss each step, after training generator and discriminator
            if not i % log_interval:  # 2x faster than testing each step...

                if DEBUG:
                    print(x.shape)
                    print(g_z.shape)


                list_log_iteration.append(log_iteration)   
     
                K.set_learning_phase(0)  # 0 = test


                x = get_data_batch(train, test_size, seed=i)
                z = np.random.normal(size=(test_size, rand_dim), scale=0.02)
                
                if with_class:
                    labels = x[:, -label_dim:]
                    g_z = generator_model.predict([z, labels])
                else:
                    g_z = generator_model.predict(z)


                xgb_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'XGB')
                xgb_losses = np.append(xgb_losses, xgb_loss)

                dt_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'DT')
                dt_losses = np.append(dt_losses, dt_loss)

                nb_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'NB')
                nb_losses = np.append(nb_losses, nb_loss)

                knn_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'KNN')
                knn_losses = np.append(knn_losses, knn_loss)

                rf_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'RF')
                rf_losses = np.append(rf_losses, rf_loss)

                lr_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'LR')
                lr_losses = np.append(lr_losses, lr_loss)

                svm_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'SVM')
                svm_losses = np.append(svm_losses, svm_loss)



                if show:
                    print('Plotting...\n')
                    PlotData(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim, save=False, prefix=data_dir + cache_prefix + '_' + str(i), list_batch_iteration=list_batch_iteration, xgb_losses= xgb_losses, svm_losses = svm_losses,  dt_losses =dt_losses, nb_losses = nb_losses, knn_losses = knn_losses, rf_losses = rf_losses, lr_losses = lr_losses, list_disc_loss_real = list_disc_loss_real, list_disc_loss_generated = list_disc_loss_generated, list_combined_loss = list_combined_loss, list_disc_perf = list_disc_perf, log_iteration = log_iteration, list_log_iteration = list_log_iteration, figs_path = FIGS_PATH)

                    # PlotData(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim, save=False, prefix=data_dir + cache_prefix + '_' + str(i), list_batch_iteration=list_batch_iteration, list_disc_loss_real = list_disc_loss_real, list_disc_loss_generated = list_disc_loss_generated, list_combined_loss = list_combined_loss, list_disc_perf = list_disc_perf)


                if DEBUG:
                    print('Step: {} of {}.'.format(i, starting_step + nb_steps))
                    # K.set_learning_phase(0) # 0 = test

                    # loss summaries
                    print('Losses: G, D Gen, D Real, XGB, XGB(min), SVM, SVM(min), DT, DT(min), NB, NB(min), KNN, KNN(min), RF, RF(min), LR, LR(min): , {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}'.format(combined_loss[-1], disc_loss_generated[-1], disc_loss_real[-1], xgb_loss, xgb_losses.min(), svm_loss, svm_losses.min(), dt_loss, dt_losses.min() , nb_loss, nb_losses.min() , knn_loss, knn_losses.min() , rf_loss, rf_losses.min() , lr_loss, lr_losses.min() ))

                    print('D Real - D Gen: {:.4f}'.format(disc_loss_real[-1] - disc_loss_generated[-1]))


                end_log_time = time.time()
                log_interval_time = end_log_time - start_log_time
                start_log_time = time.time()

                total_time_so_far += log_interval_time

                if DEBUG:
                    print("log_iteration: " + str(log_iteration) + "/" + str(nb_steps//log_interval))

                log_iteration = log_iteration + 1


                print("Time taken so far: " + str(total_time_so_far)  + " seconds")

                total_time = total_time_so_far/log_iteration * nb_steps//log_interval
                
                if DEBUG:
                    print("Average time per log_iteration: " + str(total_time_so_far/log_iteration))

                time_left = round((total_time  - total_time_so_far) / 3600 , 2)                
                time_unit = 'hours'

                if time_left < 1:
                    time_left = round(time_left * 60, 2)
                    time_unit = 'minutes'
                
                print("Time left = " + str(time_left)  + " " + time_unit)                          
                


            #---------------------------------------------------------------------------------------------------------------
            #---------------------------------------------------------------------------------------------------------------            
            #--------------------------------------------------------------------------------------------------------------- 




        # save model checkpoints
        model_checkpoint_base_name = data_dir + cache_prefix + '_{}_model_weights_step_{}.h5'
        generator_model.save_weights(model_checkpoint_base_name.format('generator', i))
        discriminator_model.save_weights(model_checkpoint_base_name.format('discriminator', i))
        pickle.dump([combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, 
                    nb_losses, knn_losses, rf_losses, lr_losses], open(data_dir + cache_prefix + '_losses_step_{}.pkl'.format(i), 'wb'))


    # log_iteration = 0

    return [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses]





    
def adversarial_training_GAN(arguments, train, data_cols, label_cols=[], seed=0, starting_step=0):

    [rand_dim, nb_steps, batch_size,
     k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
     data_dir, FIGS_PATH, generator_model_path, discriminator_model_path, loss_pickle_path, show, test_size, device] = arguments
    
    np.random.seed(seed)     # set random seed
    
    data_dim = len(data_cols)
    print('data_dim: ', data_dim)
    print('data_cols: ', data_cols)
    
    label_dim = 0
    with_class = False
    if len(label_cols) > 0: 
        with_class = True
        label_dim = len(label_cols)
        print('label_dim: ', label_dim)
        print('label_cols: ', label_cols)

    if DEBUG:

        print('with_class: ' + str(with_class))    
    
    # define network models
    
    K.set_learning_phase(1) # 1 = train
    
    if with_class:
        cache_prefix = 'CGAN'
        generator_model, discriminator_model, combined_model = define_models_CGAN(rand_dim, data_dim, label_dim, base_n_count)
    else:
        cache_prefix = 'GAN'
        generator_model, discriminator_model, combined_model = define_models_GAN(rand_dim, data_dim, base_n_count)
    
    # compile models

    adam = optimizers.Adam(lr=learning_rate, beta_1=0.5, beta_2=0.9)

    generator_model.compile(optimizer=adam, loss='binary_crossentropy')
    print(generator_model.summary())

    
    discriminator_model.compile(optimizer=adam, loss='binary_crossentropy')

    discriminator_model.trainable = False
    discriminator_model.compile(optimizer=adam, loss='binary_crossentropy')

    print(discriminator_model.summary())


    combined_model.compile(optimizer=adam, loss='binary_crossentropy')
    print(combined_model.summary())


    discriminator_model.trainable = True
    discriminator_model.compile(optimizer=adam, loss='binary_crossentropy')




    
    # if show:
        # print('-----------------------------------------------------')
        # print(generator_model.summary())
        # print(discriminator_model.summary())
        # print(combined_model.summary())

    combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses = [], [], [], [], [], [], [], [], [], []
    
    if loss_pickle_path:
        print('Loading loss pickles')
        [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses] = pickle.load(open(loss_pickle_path,'rb'))
    if generator_model_path:
        print('Loading generator model')
        generator_model.load_weights(generator_model_path, by_name=True)
    if discriminator_model_path:
        print('Loading discriminator model')
        discriminator_model.load_weights(discriminator_model_path, by_name=True)

    model_components = [ cache_prefix, with_class, starting_step,
                        train, data_cols, data_dim,
                        label_cols, label_dim,
                        generator_model, discriminator_model, combined_model,
                        rand_dim, nb_steps, batch_size, 
                        k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
                        data_dir, FIGS_PATH, generator_model_path, discriminator_model_path, show,
                        combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses,  dt_losses, 
                        nb_losses, knn_losses, rf_losses, lr_losses, test_size, device ]
        
    [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses,  dt_losses, nb_losses, knn_losses, rf_losses, lr_losses] = training_steps_GAN(model_components)
        
def training_steps_WGAN(model_components):

    [cache_prefix, with_class, starting_step,
     train, data_cols, data_dim,
     label_cols, label_dim,
     generator_model, discriminator_model, combined_model,
     rand_dim, nb_steps, batch_size,
     k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
     data_dir, FIGS_PATH, generator_model_path, discriminator_model_path, sess, _z, _x, _labels, 
     _g_z, epsilon, x_hat, gradients, _gradient_penalty, _disc_loss_generated, 
     _disc_loss_real, _disc_loss, disc_optimizer, show, combined_loss,  
     disc_loss_generated, disc_loss_real, xgb_losses, test_size, svm_losses,  dt_losses, 
                        nb_losses, knn_losses, rf_losses, lr_losses, test_size,  device] = model_components

    batch_iteration = 0
    total_time_so_far = 0

    list_batch_iteration = []

    list_log_iteration = []


    list_disc_loss_real = []
    list_disc_loss_generated = []
    list_combined_loss = []
    list_disc_perf = []

    log_iteration = 0

    start_log_time = time.time()
    #---------------------------------------------------------------------------------------------------------------
    if DEBUG:
        print("======================================================")
        print("Batch Size Selected -------->>>>>>> " + str(batch_size))
        print("======================================================")


    for i in range(starting_step, starting_step + nb_steps):
        
        with tf.device("/gpu:2"):

            K.set_learning_phase(1)  # 1 = train

            # train the discriminator
            for j in range(k_d):
                 d_l_r, d_l_g = train_discriminator_step(model_components, seed=i + j)
                
            disc_loss_real.append(d_l_r)
            disc_loss_generated.append(d_l_g)


            # train the generator
            for j in range(k_g):
                np.random.seed(i + j)
                z = np.random.normal(size=(batch_size, rand_dim))

                if with_class:
                    labels = get_data_batch(train, batch_size, seed=i + j)[:, -label_dim:]  # updated for class
                    loss = combined_model.train_on_batch([z, labels], [-np.ones(batch_size)])  # updated for class
                else:
                    loss = combined_model.train_on_batch(z, [-np.ones(batch_size)])

            combined_loss.append(loss)

            if DEBUG:
                print('Combined model trained...')



            list_batch_iteration.append(batch_iteration)


            batch_iteration = batch_iteration + 1    

            list_disc_loss_real.append(d_l_r)
            list_disc_loss_generated.append(d_l_g)

            list_combined_loss.append(loss)            
            list_disc_perf.append(d_l_r - d_l_g)            
           
#---------------------------------------------------------------------------------------------------------------
      
                                # Determine xgb loss each step, after training generator and discriminator
            if not i % log_interval:  # 2x faster than testing each step...

                list_log_iteration.append(log_iteration)   

                K.set_learning_phase(0)  # 0 = test


                x = get_data_batch(train, test_size, seed=i)
                z = np.random.normal(size=(test_size, rand_dim), scale=0.02)
                
                if with_class:
                    labels = x[:, -label_dim:]
                    g_z = generator_model.predict([z, labels])
                else:
                    g_z = generator_model.predict(z)


                if DEBUG:
                    print(x.shape)
                    print(g_z.shape)    


                xgb_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'XGB')
                xgb_losses = np.append(xgb_losses, xgb_loss)

                dt_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'DT')
                dt_losses = np.append(dt_losses, dt_loss)

                nb_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'NB')
                nb_losses = np.append(nb_losses, nb_loss)

                knn_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'KNN')
                knn_losses = np.append(knn_losses, knn_loss)

                rf_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'RF')
                rf_losses = np.append(rf_losses, rf_loss)

                lr_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'LR')
                lr_losses = np.append(lr_losses, lr_loss)

                svm_loss = CheckAccuracy(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim , classifier = 'SVM')
                svm_losses = np.append(svm_losses, svm_loss)



                if show:
                    if DEBUG:
                        print('Plotting....\n')

                    PlotData(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim, save=False, prefix=data_dir + cache_prefix + '_' + str(i), list_batch_iteration=list_batch_iteration, xgb_losses= xgb_losses, svm_losses = svm_losses,  dt_losses =dt_losses, nb_losses = nb_losses, knn_losses = knn_losses, rf_losses = rf_losses, lr_losses = lr_losses, list_disc_loss_real = list_disc_loss_real, list_disc_loss_generated = list_disc_loss_generated, list_combined_loss = list_combined_loss, list_disc_perf = list_disc_perf, log_iteration = log_iteration, list_log_iteration = list_log_iteration, figs_path= FIGS_PATH)

                    # PlotData(x, g_z, data_cols, label_cols, seed=0, with_class=with_class, data_dim=data_dim, save=False, prefix=data_dir + cache_prefix + '_' + str(i), list_batch_iteration=list_batch_iteration, list_disc_loss_real = list_disc_loss_real, list_disc_loss_generated = list_disc_loss_generated, list_combined_loss = list_combined_loss, list_disc_perf = list_disc_perf)

                if DEBUG:
                    print('Step: {} of {}.'.format(i, starting_step + nb_steps))
                    # K.set_learning_phase(0) # 0 = test

                    # loss summaries
                    print('Losses: G, D Gen, D Real, XGB, XGB(min), SVM, SVM(min), DT, DT(min), NB, NB(min), KNN, KNN(min), RF, RF(min), LR, LR(min): , {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}, {:.4f}'.format(combined_loss[-1], disc_loss_generated[-1], disc_loss_real[-1], xgb_loss, xgb_losses.min(), svm_loss, svm_losses.min(), dt_loss, dt_losses.min() , nb_loss, nb_losses.min() , knn_loss, knn_losses.min() , rf_loss, rf_losses.min() , lr_loss, lr_losses.min() ))

                    print('D Real - D Gen: {:.4f}'.format(disc_loss_real[-1] - disc_loss_generated[-1]))



                end_log_time = time.time()
                log_interval_time = end_log_time - start_log_time
                start_log_time = time.time()

                total_time_so_far += log_interval_time
                
                if DEBUG:
                    print("log_iteration: " + str(log_iteration) + "/" + str(nb_steps//log_interval))

                log_iteration = log_iteration + 1


                print("Time taken so far: " + str(total_time_so_far)  + " seconds")

                total_time = total_time_so_far/log_iteration * nb_steps//log_interval

                if DEBUG:

                    print("Average time per log_iteration: " + str(total_time_so_far/log_iteration))

                time_left = round((total_time  - total_time_so_far) / 3600 , 2)                
                time_unit = 'hours'

                if time_left < 1:
                    time_left = round(time_left * 60, 2)
                    time_unit = 'minutes'


                print("Time left = " + str(time_left)  + " " + time_unit)
                
        #---------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------            
        #--------------------------------------------------------------------------------------------------------------- 




        # save model checkpoints
        model_checkpoint_base_name = data_dir + cache_prefix + '_{}_model_weights_step_{}.h5'
        generator_model.save_weights(model_checkpoint_base_name.format('generator', i))
        discriminator_model.save_weights(model_checkpoint_base_name.format('discriminator', i))
        pickle.dump([combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, 
                    nb_losses, knn_losses, rf_losses, lr_losses], open(data_dir + cache_prefix + '_losses_step_{}.pkl'.format(i), 'wb'))


        # log_iteration = 0

    return [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses]

# # ========================================================================================================
# # ========================================================================================================
# # ========================================================================================================

# ========================================================================================================
# ========================================================================================================
# ========================================================================================================

def adversarial_training_WGAN(arguments, train, data_cols, label_cols=[], seed=0, starting_step=0):

    [rand_dim, nb_steps, batch_size,
     k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
     data_dir, FIGS_PATH, generator_model_path, discriminator_model_path, loss_pickle_path, show, test_size, device] = arguments

    np.random.seed(seed)     # set random seed

    data_dim = len(data_cols)
    print('data_dim: ', data_dim)
    print('data_cols: ', data_cols)

    label_dim = 0
    with_class = False
    if len(label_cols) > 0:
        with_class = True
        label_dim = len(label_cols)
        print('label_dim: ', label_dim)
        print('label_cols: ', label_cols)

    # define network models

    K.set_learning_phase(1)  # 1 = train

    if with_class:
        cache_prefix = 'WCGAN'
        generator_model, discriminator_model, combined_model = define_models_CGAN(rand_dim, data_dim, label_dim, base_n_count, type='Wasserstein')
    else:
        cache_prefix = 'WGAN'
        generator_model, discriminator_model, combined_model = define_models_GAN(rand_dim, data_dim, base_n_count, type='Wasserstein')

    # construct computation graph for calculating the gradient penalty
    # (improved WGAN) and training the discriminator

    _z = tf.compat.v1.placeholder(tf.float32, shape=(batch_size, rand_dim))

    _labels = None
    if with_class:
        _x = tf.compat.v1.placeholder(tf.float32, shape=(batch_size, data_dim + label_dim))
        _labels = tf.compat.v1.placeholder(tf.float32, shape=(batch_size, label_dim))  # updated for class
        _g_z = generator_model(inputs=[_z, _labels])  # updated for class
    else:
        _x = tf.compat.v1.placeholder(tf.float32, shape=(batch_size, data_dim))
        _g_z = generator_model(_z)

    epsilon = tf.compat.v1.placeholder(tf.float32, shape=(batch_size, 1))

    x_hat = epsilon * _x + (1.0 - epsilon) * _g_z
    gradients = tf.gradients(discriminator_model(x_hat), [x_hat])
    _gradient_penalty = 10.0 * tf.square(tf.norm(gradients[0], ord=2) - 1.0)

    # calculate discriminator's loss
    _disc_loss_generated = em_loss(tf.ones(batch_size), discriminator_model(_g_z))
    _disc_loss_real = em_loss(tf.ones(batch_size), discriminator_model(_x))
    _disc_loss = _disc_loss_generated - _disc_loss_real + _gradient_penalty

    # update f by taking an SGD step on mini-batch loss LD(f)
    disc_optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, beta2=0.9).minimize(_disc_loss, var_list=discriminator_model.trainable_weights)

    sess = K.get_session()

    # init = tf.global_variables_initializer()
    # sess.run(init)

    # compile models

    adam = optimizers.Adam(lr=learning_rate, beta_1=0.5, beta_2=0.9)

    print(discriminator_model.summary())
    print(generator_model.summary())


    discriminator_model.trainable = False
    combined_model.compile(optimizer=adam, loss=[em_loss])


    print(combined_model.summary())


    combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses = [], [], [], [], [], [], [], [], [], []

    model_components = [cache_prefix, with_class, starting_step,
                        train, data_cols, data_dim,
                        label_cols, label_dim,
                        generator_model, discriminator_model, combined_model,
                        rand_dim, nb_steps, batch_size,
                        k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
                        data_dir, FIGS_PATH, generator_model_path, discriminator_model_path,
                        sess, _z, _x, _labels, _g_z, epsilon, x_hat, gradients, _gradient_penalty,
                        _disc_loss_generated, _disc_loss_real, _disc_loss, disc_optimizer,
                        show, combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, test_size, svm_losses, dt_losses, nb_losses, knn_losses, rf_losses, lr_losses, test_size, device
                        ]

    if loss_pickle_path:
        print('Loading loss pickles')
        [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses] = pickle.load(open(loss_pickle_path, 'rb'))

    if generator_model_path:
        print('Loading generator model')
        generator_model.load_weights(generator_model_path)  # , by_name=True)

    if discriminator_model_path:
        print('Loading discriminator model')
        discriminator_model.load_weights(discriminator_model_path)  # , by_name=True)
    else:
        print('pre-training the critic...')
        K.set_learning_phase(1)  # 1 = train

        for i in range(critic_pre_train_steps):
            if i % 20 == 0:
                print('Step: {} of {} critic pre-training.'.format(i, critic_pre_train_steps))
            loss = train_discriminator_step(model_components, seed=i)
        if DEBUG:        
            print('Last batch of critic pre-training disc_loss: {}.'.format(loss))

    model_components = [cache_prefix, with_class, starting_step,
                        train, data_cols, data_dim,
                        label_cols, label_dim,
                        generator_model, discriminator_model, combined_model,
                        rand_dim, nb_steps, batch_size,
                        k_d, k_g, critic_pre_train_steps, log_interval, learning_rate, base_n_count,
                        data_dir, FIGS_PATH, generator_model_path, discriminator_model_path,

                        sess, _z, _x, _labels, _g_z, epsilon, x_hat, gradients, _gradient_penalty,
                        _disc_loss_generated, _disc_loss_real, _disc_loss, disc_optimizer,
                        show,
                        combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, test_size, svm_losses,  dt_losses, 
                        nb_losses, knn_losses, rf_losses, lr_losses, test_size, device
                        ]

    [combined_loss, disc_loss_generated, disc_loss_real, xgb_losses, svm_losses,  dt_losses, nb_losses, knn_losses, rf_losses, lr_losses] = training_steps_WGAN(model_components)

# ========================================================================================================

